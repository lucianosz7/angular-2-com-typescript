import { Animal } from "../Aula07-classes/Animal";
import { Dao } from "./dao"
import { Cavalo } from "../Aula07-classes/cavalo";

let dao: Dao<Animal> = new Dao<Animal>();

let animal: Animal = new Animal('Rex');
let cavalo: Cavalo = new Cavalo('Titã');

dao.insert(animal);

