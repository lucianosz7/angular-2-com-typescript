//Boolean
var isDone = false;
//Number
var decimal = 6;
var hex = 0xf00d;
var binary = 10;
var octal = 484;
//String
var color = "blue";
color = 'red';
//Template String
var fullName = "Bob Bobbingto";
var age = 37;
var sentence = "Hello, my name is " + fullName + ".\n\nI'll be " + (age + 1) + " years old next month.";
//Array
var list = [1, 2, 3];
var list2 = [1, 2, 3];
//Tuple
var x;
x = ["hello", 10];
//Erro
//x = [10, "Hello"];
console.log(x[0].substr(1));
//Enum
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
var c = Color.Green;
//Any
var notSure = 4;
notSure = "maybe a string instead";
notSure = false;
//# sourceMappingURL=basicTypes.js.map