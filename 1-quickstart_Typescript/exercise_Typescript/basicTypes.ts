//Boolean
let isDone: boolean = false;

//Number
let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;

//String
let color: string = "blue";
color = 'red';

//Template String
let fullName: string = `Bob Bobbingto`;
let age: number = 37;
let sentence: string = `Hello, my name is ${ fullName }.

I'll be ${ age + 1 } years old next month.`;

//Array
let list: number[] = [1, 2, 3];

let list2: Array<number> = [1,2,3];

//Tuple

let x: [string, number];

x = ["hello", 10];

//Erro
//x = [10, "Hello"];

console.log(x[0].substr(1));

//Enum
enum Color {Red, Green, Blue}
let c: Color = Color.Green;

//Any
let notSure: any = 4;
notSure = "maybe a string instead";
notSure = false;





